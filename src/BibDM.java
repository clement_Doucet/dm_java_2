import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer valMin = null;
        if (liste.size() != 0){
            valMin=liste.get(0);
            for (Integer i=1; i<liste.size(); i++){
                if (valMin > liste.get(i)){
                    valMin=liste.get(i);
                }
            }
            return valMin;
        }
        return valMin;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T i : liste){
            if ( i.compareTo(valeur) < 1 ){
                return false;
            }
        }
        return true;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> intersection = new ArrayList<>();
        int i = 0;
        int j = 0;
        while ( i < liste1.size() && j < liste2.size()){
            T elem1 = liste1.get(i);
            T elem2 = liste2.get(j);
            if (elem1.equals(elem2)  && !(intersection.contains(elem1))){
                intersection.add(elem1);
            }
            if (elem1.compareTo(elem2) < 0){
                i += 1;
              }
              else{
                j += 1;
              }
            }
            return intersection;
          } 


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeMots = new ArrayList<>();
        if (texte.equals(""))
          return listeMots;
        String[] mots = texte.split(" ");
        for (String mot : mots)
          if (!mot.isEmpty())
            listeMots.add(mot);
        return listeMots;
      }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if (texte.isEmpty())
        return null;
      Map<String,Integer> dictionnaire = new HashMap<>();
      List<String> listeMots = decoupe(texte);
      for (String mot : listeMots){
        if (dictionnaire.containsKey(mot)){
            dictionnaire.put(mot, dictionnaire.get(mot)+1);
        }
        else{
            dictionnaire.put(mot, 1);
        }
      }
      Collections.sort(listeMots);
      String motPlusPresent = listeMots.get(0);
      for (String mot : listeMots){
        if (dictionnaire.get(motPlusPresent) < dictionnaire.get(mot))
          motPlusPresent = mot;
      }
      return motPlusPresent;

    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int ouverte = 0;
        int fermée = 0;
        for (int i=0 ; i<chaine.length() ; i++){
          if (chaine.charAt(i) == '(')
            ouverte += 1;
          else if (chaine.charAt(i) == ')')
            fermée += 1;
          if (fermée > ouverte)
            return false;
        }
        if (fermée != ouverte)
          return false;
        return true;
  
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        return true;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
	int centre;
        int bas=0;
        int haut=liste.size()-1;	
	if (liste.size() == 0 )
            return false;
        while( bas < haut ){
            centre = ((haut + bas) / 2);
            if (valeur.compareTo(liste.get(centre)) == 0){ // si c'est egal ça renvoi 0 
                return true; 
            }

            else if(valeur.compareTo(liste.get(centre)) > 0){
                bas = centre + 1;
            }

            else{
                haut = centre;
            }
        }
        return liste.get(haut).equals(valeur);
    }



}
